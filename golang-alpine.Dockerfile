FROM golang:1.18-alpine

RUN apk update && apk add zip upx protoc git npm protoc && npm install -g aws-cdk@latest
RUN go install github.com/golang/protobuf/protoc-gen-go@latest && go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest && go install github.com/google/wire/cmd/wire@latest
