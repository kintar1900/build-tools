FROM rust:1.57-slim
WORKDIR /
RUN cargo install mdbook --no-default-features --features "search" --vers "^0.4.0"
