FROM golang:1.18-bullseye

RUN apt update && apt install -y zip upx awscli mingw-w64 libx11-dev xorg-dev freeglut3-dev npm protobuf-compiler && npm install -g aws-cdk@latest
RUN go install github.com/golang/protobuf/protoc-gen-go@latest && go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest && go install github.com/google/wire/cmd/wire@latest
